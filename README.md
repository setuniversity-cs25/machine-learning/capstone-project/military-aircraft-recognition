To run this program, necessary to place in the same folder as main file, dataset directory.
Dataset directory should contain unzipped files from "archive" (which is downloaded from Kaggle).

Note: rename directory "Horizontal Bounding Boxes" to "Horizontal_Bounding_Boxes"

Dataset is taken from here:
https://www.kaggle.com/datasets/khlaifiabilel/military-aircraft-recognition-dataset