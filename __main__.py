import os
import xml.etree.ElementTree as ET
import cv2
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten

annotation_dir = '.\dataset\Annotations\Horizontal_Bounding_Boxes'
image_dir = '.\dataset\JPEGImages'
train_file = '.\dataset\ImageSets\Main\\train.txt'
test_file = '.\dataset\ImageSets\Main\\test.txt'


max_boxes = 10


def parse_annotation(annotation_path):
    tree = ET.parse(annotation_path)
    root = tree.getroot()
    boxes = []
    for obj in root.iter('object'):
        bbox = obj.find('bndbox')
        xmin = int(bbox.find('xmin').text)
        ymin = int(bbox.find('ymin').text)
        xmax = int(bbox.find('xmax').text)
        ymax = int(bbox.find('ymax').text)
        boxes.append([xmin, ymin, xmax, ymax])
    return boxes


def load_dataset(file_path):
    with open(file_path, 'r') as f:
        image_ids = f.read().strip().split()
    images = []
    annotations = []
    for image_id in image_ids:
        image_path = os.path.join(image_dir, f'{image_id}.jpg')
        annotation_path = os.path.join(annotation_dir, f'{image_id}.xml')
        image = cv2.imread(image_path)
        boxes = parse_annotation(annotation_path)
        images.append(image)
        annotations.append(boxes)
    return images, annotations


def preprocess_data(images, annotations, input_shape=(224, 224), max_boxes=10):
    X = []
    y = []
    for img, boxes in zip(images, annotations):
        img_resized = cv2.resize(img, input_shape)
        X.append(img_resized)

        # Flatten and pad/truncate boxes
        boxes_flat = [box for sublist in boxes for box in sublist]
        if len(boxes_flat) > max_boxes * 4:
            boxes_flat = boxes_flat[:max_boxes * 4]
        else:
            boxes_flat += [0] * (max_boxes * 4 - len(boxes_flat))
        y.append(boxes_flat)

    X = np.array(X) / 255.0  # Normalize images
    y = np.array(y)
    return X, y


train_images, train_annotations = load_dataset(train_file)[:100]
test_images, test_annotations = load_dataset(test_file)[:100]

X_train, y_train = preprocess_data(train_images, train_annotations)
X_test, y_test = preprocess_data(test_images, test_annotations)


def build_model(input_shape):
    base_model = tf.keras.applications.MobileNetV2(input_shape=input_shape, include_top=False, weights='imagenet')
    x = Flatten()(base_model.output)
    x = Dense(1024, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    output = Dense(max_boxes * 4)(x)  # Adjust output to match padded box coordinates
    model = tf.keras.models.Model(inputs=base_model.input, outputs=output)
    model.compile(optimizer='adam', loss='mean_squared_error')
    return model


model = build_model((224, 224, 3))

# model.fit(X_train, y_train, epochs=10, batch_size=32, validation_data=(X_test, y_test))

early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)

model.fit(X_train, y_train, epochs=50, batch_size=16, validation_data=(X_test, y_test), callbacks=[early_stopping])


# def visualize_predictions(images, true_boxes, pred_boxes):
#     for img, t_boxes, p_boxes in zip(images, true_boxes, pred_boxes):
#         t_boxes = [t_boxes[i:i+4] for i in range(0, len(t_boxes), 4)]
#         p_boxes = [p_boxes[i:i+4] for i in range(0, len(p_boxes), 4)]
#
#         for box in t_boxes:
#             x_min, y_min, x_max, y_max = map(int, box)  # Extract coordinates
#             cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0, 255, 0), 2)
#
#         for box in p_boxes:
#             x_min, y_min, x_max, y_max = map(int, box)  # Extract coordinates
#             cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0, 0, 255), 2)
#
#         plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
#         plt.show()


def visualize_predictions(images, true_boxes, pred_boxes, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for idx, (img, t_boxes, p_boxes) in enumerate(zip(images, true_boxes, pred_boxes)):
        t_boxes = [t_boxes[i:i + 4] for i in range(0, len(t_boxes), 4)]
        p_boxes = [p_boxes[i:i + 4] for i in range(0, len(p_boxes), 4)]

        for box in t_boxes:
            x_min, y_min, x_max, y_max = map(int, box)  # Extract coordinates
            cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0, 255, 0), 2)

        for box in p_boxes:
            x_min, y_min, x_max, y_max = map(int, box)  # Extract coordinates
            cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0, 0, 255), 2)


        output_path = os.path.join(output_dir, f'prediction_{idx}.jpg')
        cv2.imwrite(output_path, img)


output_dir = './predictions'


y_pred = model.predict(X_test)


visualize_predictions(test_images, y_test, y_pred, output_dir)